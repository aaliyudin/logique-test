@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">{{ __('Register') }}</div>

        <div class="card-body">
          <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('First Name') }}</label>

              <div class="col-md-6">
                <input id="first_name" type="text" class="form-control @error('first_name') is-invalid @enderror" name="first_name" value="{{ old('first_name') }}" autocomplete="first_name" autofocus>

                @error('first_name')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Last Name') }}</label>

              <div class="col-md-6">
                <input id="last_name" type="text" class="form-control @error('last_name') is-invalid @enderror" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name" autofocus>

                @error('last_name')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Date Of Birth') }}</label>

              <div class="col-md-6">
                <input id="date_of_birth" type="date" class="form-control @error('date_of_birth') is-invalid @enderror" name="date_of_birth" value="{{ old('date_of_birth') }}" required autocomplete="date_of_birth" autofocus>

                @error('date_of_birth')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>

              <div class="col-md-6">
                <select id="gender" type="text" class="form-control @error('gender') is-invalid @enderror" name="gender" value="{{ old('gender') }}" required autocomplete="gender" autofocus>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select>
                @error('gender')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __(' Address') }}</label>

              <div class="col-md-6">
                <textarea id="address" type="address" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address"></textarea>

                @error('address')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

              <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Membership Type') }}</label>

              <div class="col-md-6">
                <select id="membership_type" type="text" class="form-control @error('membership_type') is-invalid @enderror" name="membership_type" value="{{ old('membership_type') }}" required autocomplete="membership_type" autofocus>
                  <option value="silver">Silver</option>
                  <option value="gold">Gold</option>
                  <option value="platinum">Platinum</option>
                  <option value="black">Black</option>
                  <option value="vip">VIP</option>
                  <option value="vvip">VVIP</option>
                </select>
                @error('membership_type')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-4 col-form-label text-md-right" for="card-holder-name">{{ __('Name on Card') }}</label>

              <div class="col-md-6">
                <input type="text" class="form-control @error('card-number') is-invalid @enderror" required autocomplete="card-holder-name" name="card-holder-name" id="card-holder-name">

                @error('card-holder-name')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="cc_number" class="col-md-4 col-form-label text-md-right">{{ __('Credit Card Number') }}</label>

              <div class="col-md-6">
                <div class="input-group">
                  <input id="card-number" type="number" class="form-control @error('card-number') is-invalid @enderror" name="card-number" value="{{ old('card-number') }}" required autocomplete="card-number">
                  <div class="input-group-append">
                    <span class="input-group-text text-muted">
                      <i class="fab fa-cc-visa"></i>   <i class="fab fa-cc-amex"></i>  
                      <i class="fab fa-cc-mastercard"></i>
                    </span>
                  </div>
                </div>

                @error('card-number')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-4 col-form-label text-md-right" >Expiration Date</label>
              <div class="col-md-6">
                <div class="input-group">
                  <input name="card_ex_month" required type="number" class="form-control" placeholder="MM" name="">
                  <input name="card_ex_year" required type="number" class="form-control" placeholder="YY" name="">
                </div>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-4 col-form-label text-md-right" data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
              <div class="col-md-6">
                <input type="number" class="form-control" name="card-cvv" required>
              </div>
            </div>

            <div class="form-group row">
              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

              <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
                @enderror
              </div>
            </div>

            <div class="form-group row">
              <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

              <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password">
              </div>
            </div>

            <div class="form-group form-check">
              <label class="col-md-4 col-form-label text-md-right"></label>
              <input id="terms" type="checkbox" class="@error('terms') is-invalid @enderror" name="terms" value="1" />
              <label class="form-check-label" for="terms">Agree with the terms and conditions</label>
              @error('terms')
              <span class="offset-md-4 invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>

            <div class="form-group row mb-0">
              <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                  {{ __('Register') }}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
